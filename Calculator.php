<a href="http://showerloop.org" target="_blank"><img class="imgShowerloopOrg" src="lib/illustration.png" /></a>

<?php 
// For send contrib !
if (isset($_POST['submitAddEditConf'])) {
	$message = "Contributor : ".$_POST['emailContributor'];
	$message .= "\nConfig select : ".$_POST['contribSelectConfig'];
	$message .= "\n".wordwrap($_POST['contrib'], 1000, "\r\n");
	// Envoi du mail
	mail('showerloopcalculator@zici.fr', 'Contribution showerloop calculator', $message);
}
?>

<p><?= _('Liquid freshwater represents <a href="http://www.soiron.fr/rubrique.php?id_rubrique=456" target="_blank">1%</a> of all the water on Earth.   
In the wealthiest nations of the world, a large part of the freshwater and energy consumed at home is used to shower. 
For instance, in an average french household, <a href="https://www.cieau.com/le-metier-de-leau/ressource-en-eau-eau-potable-eaux-usees/quels-sont-les-usages-domestiques-de-leau/" target="_blank">39%</a> of this water is used for bathing and showering and hot water represents <a href="https://www.ademe.fr/particuliers-eco-citoyens/habitation/bien-gerer-habitat/leau-chaude-sanitaire" target="_blank">12%</a> of the energy consumption. 
It is therefore judicious to think about ways to reduce this consumption. 
The <a href="http://showerloop.org" target="_blank">showerloop</a> is a water recycling shower : water is in a closed loop, it fall again on my head once filtered and therefore clean. 
It is adequate in terms of water and energy savings, and the following calculator will illustrate this') ?> :</p>

<?php
	// Scan conf
	$cdir = scandir('./config/');
	foreach ($cdir as $name)
	{
	   if (preg_match('/^config-[a-zA-Z]/', $name)) {
		   $isoCode = explode('-', $name);
		   $isoCode = explode('.', $isoCode[1]);
		   $configs_dispo[$isoCode[0]] = parse_ini_file('./config/'.$name, true);
	   }
	}
?>
<script src="./lib/Chart.min.js"></script>
<?php 
/*
 * ####### Formulaire #######
*/
?>
<form method="get" action="#" id="ShowerCalculator">

	<div class="" id="BlocIntro">
		
		<div class="form selectConfig">
			<form method="get" action="#" name="formSelectConfig" id="formSelectConfig">
			<label><?= _('Select your country\'s configuration') ?> : </label>
		    <select class="shared" id="selectConfig" name="selectConfig">
				<?php 
				foreach ($configs_dispo as $config_dispo) {
					echo '<option value="'.$config_dispo['general']['code'].'"';
					if ($_GET['selectConfig'] == $config_dispo['general']['code'] ||
						$config_use == $config_dispo['general']['code']) {
						echo ' selected="selected"';
					}							
					echo '>'.$config_dispo['general']['name'].'</option>';
					echo "\n";
				}
				?>
			</select>
			<span id="contrib">
			<a id="addConf" href="#"><?= _('Contribute by adding your country\'s configuration') ?></a>
			<a id="editConf"  href="#"><?= _('Change this country\'s configuration') ?></a>
			</span>
			</form>
		</div>

		<form method="post" action="#" id="formAddEditConf">
			<h2 id="txtAddConf"><?= _('Contribute by adding your country\'s configuration') ?></h2>
			<h2 id="txtEditConf"><?= _('Change this country\'s configuration') ?></h2>
			<label><?= _('Your email') ?></label>
			<input type="emailContributor" name="emailContributor" />
			<p id="FormContribSelectConfig">
			<label><?= _('Select your country\'s configuration') ?> : </label>
			<select name="contribSelectConfig" id="contribSelectConfig">
				<?php 
				foreach ($configs_dispo as $config_dispo) {
					echo '<option value="'.$config_dispo['general']['code'].'"';
					if ($_GET['selectConfig'] == $config_dispo['general']['code'] ||
						$config_use == $config_dispo['general']['code']) {
						echo ' selected="selected"';
					}							
					echo '>'.$config_dispo['general']['name'].'</option>';
					echo "\n";
				}
				?>
			</select></p>
			<p><textarea rows="8" cols="70" name="contrib"><?= _('Please indicate all or part of the information you want to add / modify

Country name :
Currency :
Water price :
Wastewater price :
Cold water supply temperature (°C) :
Avenage number of person in household :
Avenage number of showers per person per week :
Average time spent under the shower per person (min) :
Average showering water temperature (°C) :
Avenage flow rate of your shower (L/min) :
Most common hot water system :

We will process and integrate them as soon as possible,
Thank you for your contribution') ?>
</textarea></p>
			<p><input name="submitAddEditConf" type="submit" value="<?= _('Suggest / Contribute') ?>" /></p>
		</form>

		<form method="get" action="#" id="ShowerCalculator">
			
		<div class="form Verbose">
			<label><?= _('Choose the level of detail') ?> : </label>
			<select class="shared" id="Verbose" name="Verbose">
				<option value="1"<?php echo valeurRecupSelect('Verbose', 1); ?>><?= _('Minimum') ?></option>
				<option value="2"<?php echo valeurRecupSelect('Verbose', 2); ?>><?= _('Intermediate') ?></option>
				<option value="3"<?php echo valeurRecupSelect('Verbose', 3); ?>><?= _('Maximum') ?></option>
			</select>
		</div>
	</div>
	
	<div class="blocs infos" id="BlocData">
		<h2 class="titre data"><?= _('Data') ?> :</h2>	

			<div class="form YieldInstallHotWater">
				<label><?= _('Water heater efficiency (heat losses)') ?> : </label>
				<input class="shared" maxlength="4" size="4" id="YieldInstallHotWater" type="number" step="1" min="0" max="100" style="width: 100px;" value="<?php echo valeurRecup('YieldInstallHotWater'); ?>" name="YieldInstallHotWater" />
			</div>
			
			<p class="EnergyTheoretical1l1degForShower" style="display: none">
				<span id="EnergyTheoretical1l1degForShower">?</span>Wh
			(?)</p>
			
			<div class="form WaterTemp">
				<label><?= _('Cold water supply temperature') ?> :</label>
				<input class="shared" id="WaterTemp" type="number" step="0.1" min="-50" max="999" style="width: 100px;" value="<?php echo valeurRecup('WaterTemp'); ?>" name="WaterTemp" /> °C
			</div>
			
			<div class="form WaterCostShop">
				<label><?= _('Water price') ?> :</label>
				<input class="shared" id="WaterCostShop" type="number" step="0.01"  min="0" max="99999" style="width: 100px;" value="<?php echo valeurRecup('WaterCostShop'); ?>" name="WaterCostShop" /> <?= $config_ini['general']['currency'] ?>/m&sup3;
			</div>
			
			<div class="form CleanWaterCost">
				<label><?= _('Wastewater price') ?> :</label>
				<input class="shared" id="CleanWaterCost" type="number" step="0.01"  min="0" max="99999" style="width: 100px;" value="<?php echo valeurRecup('CleanWaterCost'); ?>" name="CleanWaterCost" /> <?= $config_ini['general']['currency'] ?>/m&sup3;
			</div>
			
			<p><?= _('Water total cost :') ?> <span id="WaterCost"></span> <?= $config_ini['general']['currency'] ?>/m&sup3; <a rel="tooltip" id="WaterCostTooltip" class="bulles" title="?">(?)</a></p>
			
	</div>
	
	<div class="blocs infos" id="BlocDesc">
		<h2 class="titre you"><?= _('Description of your household') ?> :</h2>	
				
				
			<div class="form NbPeaple">
				<label><?= _('Number of persons in the household') ?> :</label>
				<input class="shared" id="NbPeaple" type="number" min="1" max="999999999" style="width: 100px;" value="<?php echo valeurRecup('NbPeaple'); ?>" name="NbPeaple" />
			</div>
			
			<div class="form NbShowerPerPeaplePerWeek">
				<label><?= _('Number of showers per person per week') ?> :</label>
				<input class="shared" id="NbShowerPerPeaplePerWeek" type="number" min="1" max="999999999" style="width: 100px;" value="<?php echo valeurRecup('NbShowerPerPeaplePerWeek'); ?>" name="NbShowerPerPeaplePerWeek" /> 
			</div>
			
			<div class="form ShowerTimePerPeaple">
				<label><?= _('Average time spent under the shower per person') ?> :</label>
				<input class="shared" id="ShowerTimePerPeaple" type="number" min="1" max="9999" style="width: 100px;" value="<?php echo valeurRecup('ShowerTimePerPeaple'); ?>" name="ShowerTimePerPeaple" /> min
			</div>
			
			<div class="form ShowerTemp">
				<label><?= _('Average showering water temperature') ?> :</label>
				<input class="shared" id="ShowerTemp" type="number" min="1" max="999999999" style="width: 100px;" value="<?php echo valeurRecup('ShowerTemp'); ?>" name="ShowerTemp" /> °C
			</div>
			
			<div class="form HotWaterSystem">
				<label><?= _('You\'re current water heating system') ?> :</label>
				<select class="shared" name="HotWaterSystem" id="HotWaterSystem">				
					<?php
					// Switch to default energy if enrgy not disponible in this config
					if (empty($config_ini['energy'])) {
						$config_ini['energy']=$config_ini_default['energy'];
					}
					foreach($config_ini['energy'] as $energykey => $energy) {
						echo '<option value="'.$energykey.'" ';
						valeurRecupSelect('HotWaterSystem', $energykey);
						echo '>'.$energy['name'].'</option>';
					}
					?>
				</select>
				<?php
				foreach($config_ini['energy'] as $energykey => $energy) {
					echo '<input name="PriceEnergy'.$energykey.'" id="PriceEnergy'.$energykey.'" value="'.$energy['price'].'"  type="hidden" />';
				}
				?>
				<p id="txtWattCost"><?= _('The cost of this energy is') ?> <span id="WattCost">?</span> <?= $config_ini['general']['currency'] ?>/kWh</p>
			</div>
		</div> 
		
		<div class="blocs infos" id="BlocShower">
			<h2 class="titre you"><?= _('With your current shower') ?> :</h2>	
			
			<div class="form ShowerFlow"é>
				<label><?= _('Flow rate of your shower') ?> :</label>
				<input class="shared" id="ShowerFlow" type="number" min="1" max="99" style="width: 100px;" value="<?php echo valeurRecup('ShowerFlow'); ?>" name="ShowerFlow" /> L/min 
				<!--Creates the popup body-->
				<div class="popup-overlay">
				  <!--Creates the popup content-->
				   <div class="popup-content">
					  <h2><?= _('How to measure how much water flows from your shower every minute.'); ?></h2>
						<p><?= _('You will need :') ?></p>
						<ul>
							<li><?= _('One standard household bucket'); ?></li>
							<li><?= _('One measuring jug with litre gradations') ?></li>
							<li><?= _('A timer or watch with a second hand') ?></li>
						</ul>
						<ol>
							<li><?= _('Turn on the hot water fully, and then adjust the cold tap to ensure the shower is at your preferred temperature.') ?></li>
							<li><?= _('Catch the water in the bucket for exactly 20 seconds. Ensure you catch all the water, don\'t allow water to spray wide of the bucket. Remove it from the stream as soon as 20 seconds is up.') ?></li>
							<li><?= _('Pour the contents of the bucket into the measuring jug. Then multiply the total amount of water you collected by 3 and this will give you the flow rate of your shower in litres per minute.') ?></li>
							<li><?= _('Obviously, do not throw away the water collected. Use it to wash the floor, water plants...') ?></li>
						</ol>
					  <a class="close"><?= _('Close') ?></a>    
				  </div>
				</div>

				<a class="open">?</a>
				<small>(<?= _('8L/min for an eco, 20L/min for a regular showerhead (<a href=\'https://www.jeconomiseleau.org/index.php/particuliers/economies-par-usage/la-douche-et-le-bain\'>source</a>)') ?>)</small>
			</div>
			
			<p id="txtWaterPerShower"><?= _('You’re using <b><span id="WaterPerShower">?</span>L of water</b> per shower, meaning') ?> <span id="CostWaterPerShower">?</span><?= $config_ini['general']['currency'] ?></p>
			<p id="txtWattPerShower"><?= _('You need <b><span id="WattPerShower">?</span>kWh of energy</b> per shower, meaning') ?> <span id="CostWattPerShower">?</span><?= $config_ini['general']['currency'] ?></p>
			<p id="txtCostGlobalPerShower"><?= _('One shower cost you') ?><b> <span id="CostGlobalPerShower">?</span><?= $config_ini['general']['currency'] ?></b></p>
			
			<h3 id="txtShowerPerYear"><?= _('Yearly, for your household') ?></h3>
			<p><?= _('You’re using <b><span id="WaterShowerPerYear">?</span>L of water</b> to shower, meaning') ?> <span id="CostWaterShowerPerYear">?</span><?= $config_ini['general']['currency'] ?></p>
			<p><?= _('You need <b><span id="WattShowerPerYear">?</span>kWh of energy</b>, meaning') ?> <span id="CostWattShowerPerYear">?</span><?= $config_ini['general']['currency'] ?></p>
			<p><b><?= _('You have an annuel budget estimated to be around ') ?><span id="CostGlobalShowerPerYear">?</span><?= $config_ini['general']['currency'] ?></b></p>
			
		</div>
		
		<div class="blocs infos" id="BlocShowerloop">
			<h2 class="titre you"><?= _('If you had a showerloop') ?> :</h2>	
			<div id="txtShowerloopModel" class="form ShowerloopModel">
				<label><?= _('You would have the model') ?> :</label>
				<select class="shared" name="ShowerloopModel" id="ShowerloopModel">
					<?php 
					foreach ($config_ini_default['showerloop'] as $showerloopKey => $showerloopModel) {	
						echo '<option value="'.$showerloopKey.'|'.$showerloopModel['BuyingPrice'].'|'.$showerloopModel['AnnualMaintenancePrice'].'|'.$showerloopModel['waterConsumption'].'|'.$showerloopModel['powerConsumption'].'|'.$showerloopModel['pumpFlow'].'">'.$showerloopModel['name'].'</option>';
					}
					?>
				</select>
				<p><?= _('Total cost of the showerloop is') ?> <span id="ShowerloopBuyPrice">?</span><?= $config_ini['general']['currency'] ?> <?= _('Maintenance annual cost (replacement of wearing parts) is') ?> <span id="ShowerloopAnnualMaintenancePrice">?</span><?= $config_ini['general']['currency'] ?></p>
				<input type="hidden" id="WattPerShowerloop" />
				<input type="hidden" id="pumpFlow" />
			</div>
			
			<p id="txtWaterPerShowerloop"><?= _('This model of showerloop uses <b><span id="WaterPerShowerloop">?</span>L of water</b>, meaning') ?> <span id="CostWaterPerShowerloop">?</span><?= $config_ini['general']['currency'] ?></p>
			<p><?= _('You would save <b><span id="WaterPerShowerCompare">?</span>% of water</b> compared to a typical shower') ?>
			<p id="txtWattPerShowerloopHotWater"><?= _('You would need <span id="WattPerShowerloopHotWater">?</span>kWh of energy to heat the water') ?></p>
			<p id="txtWattPerShowerloopOperation"><?= _('You would need <span id="WattPerShowerloopOperation">?</span>kWh of energy to run the showerloop') ?></p>
			<p id="txtWattPerShowerloopTotal"><?= _('You would need <b><span id="WattPerShowerloopTotal">?</span>kWh of energy</b> per shower meaning') ?> <span id="CostWattPerShowerloop">?</span><?= $config_ini['general']['currency'] ?></p>
			<p><?= _('You would save <b><span id="WattPerShowerCompare">?</span>% of energy</b> compared to a typical shower') ?>
			<p id="txtCostGlobalPerShowerloop"><?= _('A shower would cost you') ?> <b><span id="CostGlobalPerShowerloop">?</span><?= $config_ini['general']['currency'] ?></b></p>
			
			<h3 id="txtShowerloopPerYear"><?= _('Yearly, for your household') ?></h3>
			<p><?= _('You would use <b><span id="WaterShowerloopPerYear">?</span>L of water</b> per year to shower, meaning') ?> <span id="CostWaterShowerloopPerYear">?</span><?= $config_ini['general']['currency'] ?></p>
			<p><?= _('You would need <b><span id="WattShowerloopPerYear">?</span>kWh of energy</b> per year to shower, meaning') ?> <span id="CostWattShowerloopPerYear">?</span><?= $config_ini['general']['currency'] ?></p>
			<p><b><?= _('You would have an annuel budget estimated to be around') ?> <span id="CostGlobalShowerloopPerYear">?</span><?= $config_ini['general']['currency'] ?></b></p>
			<p id="txtRoi"><?= _('The purchase of the equipment would pay for itself in <span id="roi">?</span> year(s)') ?> </p>
			
		</div>
		
		
		<div class="blocs char" id="BlocChar">
			<div class="chart-container"><canvas id="WaterChar"></canvas></div>
			<script>
			var ctx = document.getElementById("WaterChar").getContext('2d');
			var WaterChar = new Chart(ctx, {
				type: 'line',
				data: {
					labels: ['?', '?', '?', '?', '?'],
					datasets: [{
						label: '<?= _('Showerloop') ?>',
						data: ['O', '0', '0', '0', '0'],
						backgroundColor: 'rgba(86, 93, 255, 0.8)',
						borderColor: 'rgba(86, 93, 255, 1)',
						borderWidth: 1
					},{
						label: '<?= _('Typical shower') ?>',
						data: ['O', '0', '0', '0', '0'],
						backgroundColor: 'rgba(255, 1, 86, 0.8)',
						borderColor: 'rgba(255, 2, 64, 1)',
						borderWidth: 1
					}]
				},
				options: {
					animation: {
						duration: 0
					},
					 title: {
						display: true,
						text: "<?= _('Total water consumption for the household per year for showering') ?>"
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true,
								callback: function(label, index, labels) {
									return label+'m3';
								}
							},
						}],
						xAxes: [{
							scaleLabel: {
									display: true,
									labelString: "<?= _('Years') ?>"
							}
						}]
					}
				}
			});

			</script>
		

			<div class="chart-container"><canvas id="WattChar"></canvas></div>
			<script>
			var ctx = document.getElementById("WattChar").getContext('2d');
			var WattChar = new Chart(ctx, {
				type: 'line',
				data: {
					labels: ['?', '?', '?', '?', '?'],
					datasets: [{
						label: '<?= _('Showerloop') ?>',
						data: ['O', '0', '0', '0', '0'],
						backgroundColor: 'rgba(86, 93, 255, 0.8)',
						borderColor: 'rgba(86, 93, 255, 1)',
						borderWidth: 1
					},{
						label: '<?= _('Typical shower') ?>',
						data: ['O', '0', '0', '0', '0'],
						backgroundColor: 'rgba(255, 1, 86, 0.8)',
						borderColor: 'rgba(255, 2, 64, 1)',
						borderWidth: 1
					}]
				},
				options: {
					animation: {
						duration: 0
					},
					 title: {
						display: true,
						text: "<?= _('Total energy consumption for the household per year for showering') ?>"
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true,
								callback: function(label, index, labels) {
									return label+'kWh';
								}
							},
						}],
						xAxes: [{
							scaleLabel: {
									display: true,
									labelString: "<?= _('Years') ?>"
							}
						}]
					}
				}
			});

			</script>

			<div class="chart-container"><canvas id="CostChar"></canvas></div>
			<script>
			var ctx = document.getElementById("CostChar").getContext('2d');
			var CostChar = new Chart(ctx, {
				type: 'line',
				data: {
					labels: ['?', '?', '?', '?', '?'],
					datasets: [{
						label: '<?= _('Showerloop (1)') ?>',
						data: ['O', '0', '0', '0', '0'],
						backgroundColor: 'rgba(86, 93, 255, 0.8)',
						borderColor: 'rgba(86, 93, 255, 1)',
						borderWidth: 1
					},{
						label: '<?= _('Typical shower') ?>',
						data: ['O', '0', '0', '0', '0'],
						backgroundColor: 'rgba(255, 1, 86, 0.8)',
						borderColor: 'rgba(255, 2, 64, 1)',
						borderWidth: 1
					}]
				},
				options: {
					animation: {
						duration: 0
					},
					 title: {
						display: true,
						text: "<?= _('Shower budget for the household per year') ?>"
					},
					scales: {
						xAxes: [{
							scaleLabel: {
									display: true,
									labelString: "<?= _('Years') ?>"
							}
						}]
					}
				}
			});

			</script>
			<p style="text-align: right"><small><?= _('(1) Showerloop with investment and maintenance cost') ?></small></p>
		</div>
		
		<input type="button" name="shareButton" id="shareButton" value="<?= _('Share this results') ?>"/>
		<input type="text" name="shareUrl" id="shareUrl" value="" Onclick="this.select();;document.execCommand('copy');" />
		
		
        <div id="shareSocial">
        <!-- share https://sharingbuttons.io/ -->
        
		<!-- Sharingbutton E-Mail -->
		<a class="resp-sharing-button__link email" href="#" target="_self" aria-label="">
		  <div class="resp-sharing-button resp-sharing-button--email resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm8 16c0 1.1-.9 2-2 2H6c-1.1 0-2-.9-2-2V8c0-1.1.9-2 2-2h12c1.1 0 2 .9 2 2v8z"/><path d="M17.9 8.18c-.2-.2-.5-.24-.72-.07L12 12.38 6.82 8.1c-.22-.16-.53-.13-.7.08s-.15.53.06.7l3.62 2.97-3.57 2.23c-.23.14-.3.45-.15.7.1.14.25.22.42.22.1 0 .18-.02.27-.08l3.85-2.4 1.06.87c.1.04.2.1.32.1s.23-.06.32-.1l1.06-.9 3.86 2.4c.08.06.17.1.26.1.17 0 .33-.1.42-.25.15-.24.08-.55-.15-.7l-3.57-2.22 3.62-2.96c.2-.2.24-.5.07-.72z"/></svg>
			</div>
		  </div>
		</a>
		
        <!-- Sharingbutton Facebook -->
		<a class="resp-sharing-button__link facebook" href="#" target="_blank" aria-label="">
		  <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm3.6 11.5h-2.1v7h-3v-7h-2v-2h2V8.34c0-1.1.35-2.82 2.65-2.82h2.35v2.3h-1.4c-.25 0-.6.13-.6.66V9.5h2.34l-.24 2z"/></svg>
			</div>
		  </div>
		</a>

		<!-- Sharingbutton Twitter -->
		<a class="resp-sharing-button__link twitter" href="#" target="_blank" aria-label="">
		  <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm5.26 9.38v.34c0 3.48-2.64 7.5-7.48 7.5-1.48 0-2.87-.44-4.03-1.2 1.37.17 2.77-.2 3.9-1.08-1.16-.02-2.13-.78-2.46-1.83.38.1.8.07 1.17-.03-1.2-.24-2.1-1.3-2.1-2.58v-.05c.35.2.75.32 1.18.33-.7-.47-1.17-1.28-1.17-2.2 0-.47.13-.92.36-1.3C7.94 8.85 9.88 9.9 12.06 10c-.04-.2-.06-.4-.06-.6 0-1.46 1.18-2.63 2.63-2.63.76 0 1.44.3 1.92.82.6-.12 1.95-.27 1.95-.27-.35.53-.72 1.66-1.24 2.04z"/></svg>
			</div>
		  </div>
		</a>

		<!-- Sharingbutton Google+ -->
		<a class="resp-sharing-button__link google" href="#" target="_blank" aria-label="">
		  <div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12.65 8.6c-.02-.66-.3-1.3-.8-1.8S10.67 6 9.98 6c-.63 0-1.2.25-1.64.68-.45.44-.68 1.05-.66 1.7.02.68.3 1.32.8 1.8.96.97 2.6 1.04 3.5.14.45-.45.7-1.05.67-1.7zm-2.5 5.63c-2.14 0-3.96.95-3.96 2.1 0 1.12 1.8 2.08 3.94 2.08s3.98-.93 3.98-2.06c0-1.14-1.82-2.1-3.98-2.1z"/><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm-1.84 19.4c-2.8 0-4.97-1.35-4.97-3.08s2.15-3.1 4.94-3.1c.84 0 1.6.14 2.28.36-.57-.4-1.25-.86-1.3-1.7-.26.06-.52.1-.8.1-.95 0-1.87-.38-2.57-1.08-.67-.68-1.06-1.55-1.1-2.48-.02-.94.32-1.8.96-2.45.65-.63 1.5-.93 2.4-.92V5h3.95v1h-1.53l.12.1c.67.67 1.06 1.55 1.1 2.48.02.93-.32 1.8-.97 2.45-.16.15-.33.3-.5.4-.2.6.05.8.83 1.33.9.6 2.1 1.42 2.1 3.56 0 1.73-2.17 3.1-4.96 3.1zM20 10h-2v2h-1v-2h-2V9h2V7h1v2h2v1z"/></svg>
			</div>
		  </div>
		</a>

		<!-- Sharingbutton Pinterest -->
		<a class="resp-sharing-button__link pinterest" href="#" target="_blank" aria-label="">
		  <div class="resp-sharing-button resp-sharing-button--pinterest resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm1.4 15.56c-1 0-1.94-.53-2.25-1.14l-.65 2.52c-.4 1.45-1.57 2.9-1.66 3-.06.1-.2.07-.22-.04-.02-.2-.32-2 .03-3.5l1.18-5s-.3-.6-.3-1.46c0-1.36.8-2.37 1.78-2.37.85 0 1.25.62 1.25 1.37 0 .85-.53 2.1-.8 3.27-.24.98.48 1.78 1.44 1.78 1.73 0 2.9-2.24 2.9-4.9 0-2-1.35-3.5-3.82-3.5-2.8 0-4.53 2.07-4.53 4.4 0 .5.1.9.25 1.23l-1.5.82c-.36-.64-.54-1.43-.54-2.28 0-2.6 2.2-5.74 6.57-5.74 3.5 0 5.82 2.54 5.82 5.27 0 3.6-2 6.3-4.96 6.3z"/></svg>
			</div>
		  </div>
		</a>

		<!-- Sharingbutton Reddit -->
		<a class="resp-sharing-button__link reddit" href="" target="_blank" aria-label="">
		  <div class="resp-sharing-button resp-sharing-button--reddit resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><circle cx="9.391" cy="13.392" r=".978"/><path d="M14.057 15.814c-1.14.66-2.987.655-4.122-.004-.238-.138-.545-.058-.684.182-.13.24-.05.545.19.685.72.417 1.63.646 2.568.646.93 0 1.84-.228 2.558-.642.24-.13.32-.44.185-.68-.14-.24-.445-.32-.683-.18zM5 12.086c0 .41.23.78.568.978.27-.662.735-1.264 1.353-1.774-.2-.207-.48-.334-.79-.334-.62 0-1.13.507-1.13 1.13z"/><path d="M12 0C5.383 0 0 5.383 0 12s5.383 12 12 12 12-5.383 12-12S18.617 0 12 0zm6.673 14.055c.01.104.022.208.022.314 0 2.61-3.004 4.73-6.695 4.73s-6.695-2.126-6.695-4.74c0-.105.013-.21.022-.313C4.537 13.73 4 12.97 4 12.08c0-1.173.956-2.13 2.13-2.13.63 0 1.218.29 1.618.757 1.04-.607 2.345-.99 3.77-1.063.057-.803.308-2.33 1.388-2.95.633-.366 1.417-.323 2.322.085.302-.81 1.076-1.397 1.99-1.397 1.174 0 2.13.96 2.13 2.13 0 1.177-.956 2.133-2.13 2.133-1.065 0-1.942-.79-2.098-1.81-.734-.4-1.315-.506-1.716-.276-.6.346-.818 1.395-.88 2.087 1.407.08 2.697.46 3.728 1.065.4-.468.987-.756 1.617-.756 1.17 0 2.13.953 2.13 2.13 0 .89-.54 1.65-1.33 1.97z"/><circle cx="14.609" cy="13.391" r=".978"/><path d="M17.87 10.956c-.302 0-.583.128-.79.334.616.51 1.082 1.112 1.352 1.774.34-.197.568-.566.568-.978 0-.623-.507-1.13-1.13-1.13z"/></svg>
			</div>
		  </div>
		</a>

		<!-- Sharingbutton WhatsApp -->
		<a class="resp-sharing-button__link watsapp" href="#" target="_blank" aria-label="">
		  <div class="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
			<svg xmlns="http://www.w3.org/2000/svg" height="24" width="24" viewBox="0 0 24 24"><path d="m12 0c-6.6 0-12 5.4-12 12s5.4 12 12 12 12-5.4 12-12-5.4-12-12-12zm0 3.8c2.2 0 4.2 0.9 5.7 2.4 1.6 1.5 2.4 3.6 2.5 5.7 0 4.5-3.6 8.1-8.1 8.1-1.4 0-2.7-0.4-3.9-1l-4.4 1.1 1.2-4.2c-0.8-1.2-1.1-2.6-1.1-4 0-4.5 3.6-8.1 8.1-8.1zm0.1 1.5c-3.7 0-6.7 3-6.7 6.7 0 1.3 0.3 2.5 1 3.6l0.1 0.3-0.7 2.4 2.5-0.7 0.3 0.099c1 0.7 2.2 1 3.4 1 3.7 0 6.8-3 6.9-6.6 0-1.8-0.7-3.5-2-4.8s-3-2-4.8-2zm-3 2.9h0.4c0.2 0 0.4-0.099 0.5 0.3s0.5 1.5 0.6 1.7 0.1 0.2 0 0.3-0.1 0.2-0.2 0.3l-0.3 0.3c-0.1 0.1-0.2 0.2-0.1 0.4 0.2 0.2 0.6 0.9 1.2 1.4 0.7 0.7 1.4 0.9 1.6 1 0.2 0 0.3 0.001 0.4-0.099s0.5-0.6 0.6-0.8c0.2-0.2 0.3-0.2 0.5-0.1l1.4 0.7c0.2 0.1 0.3 0.2 0.5 0.3 0 0.1 0.1 0.5-0.099 1s-1 0.9-1.4 1c-0.3 0-0.8 0.001-1.3-0.099-0.3-0.1-0.7-0.2-1.2-0.4-2.1-0.9-3.4-3-3.5-3.1s-0.8-1.1-0.8-2.1c0-1 0.5-1.5 0.7-1.7s0.4-0.3 0.5-0.3z"/></svg>
			</div>
		  </div>
		</a>

		<!-- Sharingbutton Hacker News -->
		<a class="resp-sharing-button__link ycombinator" href="#" target="_blank" aria-label="">
		  <div class="resp-sharing-button resp-sharing-button--hackernews resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 256"><path fill-rule="evenodd" d="M128 256c70.692 0 128-57.308 128-128C256 57.308 198.692 0 128 0 57.308 0 0 57.308 0 128c0 70.692 57.308 128 128 128zm-9.06-113.686L75 60h20.08l25.85 52.093c.397.927.86 1.888 1.39 2.883.53.994.995 2.02 1.393 3.08.265.4.463.764.596 1.095.13.334.262.63.395.898.662 1.325 1.26 2.618 1.79 3.877.53 1.26.993 2.42 1.39 3.48 1.06-2.254 2.22-4.673 3.48-7.258 1.26-2.585 2.552-5.27 3.877-8.052L161.49 60h18.69l-44.34 83.308v53.087h-16.9v-54.08z"/></svg>
			</div>
		  </div>
		</a>

		<!-- Sharingbutton Telegram -->
		<a class="resp-sharing-button__link telegram" href="#" target="_blank" aria-label="">
		  <div class="resp-sharing-button resp-sharing-button--telegram resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 23.5c6.35 0 11.5-5.15 11.5-11.5S18.35.5 12 .5.5 5.65.5 12 5.65 23.5 12 23.5zM2.505 11.053c-.31.118-.505.738-.505.738s.203.62.513.737l3.636 1.355 1.417 4.557a.787.787 0 0 0 1.25.375l2.115-1.72a.29.29 0 0 1 .353-.01L15.1 19.85a.786.786 0 0 0 .746.095.786.786 0 0 0 .487-.573l2.793-13.426a.787.787 0 0 0-1.054-.893l-15.568 6z" fill-rule="evenodd"/></svg>
			</div>
		  </div>
		</a>
		</div>
		
</form>

<script type="text/javascript">
	


//appends an "active" class to .popup and .popup-content when the "Open" button is clicked
$(".open").on("click", function(){
  $(".popup-overlay, .popup-content").addClass("active");
});

//removes the "active" class to .popup and .popup-content when the "Close" button is clicked 
$(".close, .popup-overlay").on("click", function(){
  $(".popup-overlay, .popup-content").removeClass("active");
});



	
// Share URL Boutton
$('#shareButton').on('click', function() {
	$( "#shareUrl" ).show();
	$( "#shareSocial" ).show();
	
	// Liste le formulaire 
	var URLconstruction = null;
	
	var inputs, index;
	inputs = document.getElementsByClassName('shared');
	var error=0;
	for (index = 0; index < inputs.length; ++index) {
		if (inputs[index].value > 0 ||
			inputs[index].value != null ||
			inputs[index].value != '') {
				if (URLconstruction == null) {
					URLconstruction='?';
				} else {
					URLconstruction=URLconstruction+'&';
				}
				URLconstruction=URLconstruction + inputs[index].name +'='+ inputs[index].value;
		} 
	}
	
	$( "#shareUrl" ).val('<?= $ShowerloopCalculatorURL ?>'+URLconstruction);
	UrlForShare=encodeURIComponent("<?= $ShowerloopCalculatorURL ?>"+URLconstruction);
	
	// Social button link
	$('.resp-sharing-button__link.email').attr("href", "mailto:?subject=Showerloop%20Calculator&body="+UrlForShare);
	$('.resp-sharing-button__link.facebook').attr("href", "https://facebook.com/sharer/sharer.php?u="+UrlForShare);
	$('.resp-sharing-button__link.twitter').attr("href", "https://twitter.com/intent/tweet/?text=Showerloop%20Calculator&url="+UrlForShare);
	$('.resp-sharing-button__link.twitter').attr("href", "https://twitter.com/intent/tweet/?text=Showerloop%20Calculator&url="+UrlForShare);
	$('.resp-sharing-button__link.google').attr("href", "https://plus.google.com/share?url="+UrlForShare);
	$('.resp-sharing-button__link.pinterest').attr("href", "https://pinterest.com/pin/create/button/?url="+UrlForShare+"&description=Showerloop%20Calculator");
	$('.resp-sharing-button__link.reddit').attr("href", "https://reddit.com/submit/?url="+UrlForShare);
	$('.resp-sharing-button__link.whatsapp').attr("href", "whatsapp://send?text=Showerloop%20Calculator="+UrlForShare);
	$('.resp-sharing-button__link.ycombinator').attr("href", "https://news.ycombinator.com/submitlink?t=Showerloop%20Calculator&u="+UrlForShare);
	$('.resp-sharing-button__link.telegram').attr("href", "https://telegram.me/share/url?text=Showerloop%20Calculator&url="+UrlForShare);
});
// For char		
function removeData(chart) {
	chart.data.labels.pop();
	chart.data.datasets.forEach((dataset) => {
		dataset.data.pop();
	});
	chart.update();
}
function addData(chart, label, dataShower, dataShowerloop) {
	chart.data.labels.push(label);
	chart.data.datasets[0].data.push(dataShowerloop);
	chart.data.datasets[1].data.push(dataShower);
	chart.update();
}
$( "#Verbose" ).change(function () {
	changeNiveau();
});
$( "#selectConfig" ).change(function () {
	this.form.submit();
});
$( "#HotWaterSystem" ).change(function () {
	init();
});
$( "#ShowerloopModel" ).change(function () {
	init();
});
$( "#editConf" ).click(function () {
	$( "#formAddEditConf" ).show();
	$( "#FormContribSelectConfig" ).show();
	$( "#contribSelectConfig" ).prop( "disabled", false );
	$( "#txtEditConf" ).show();
	$( "#txtAddConf" ).hide();
	
});
$( "#addConf" ).click(function () {
	$( "#formAddEditConf" ).show();
	$( "#FormContribSelectConfig" ).hide();
	$( "#contribSelectConfig" ).prop( "disabled", true );
	$( "#txtAddConf" ).show();
	$( "#txtEditConf" ).hide();
});


$("input").change(function () {
/*
	// Détextion des erreurs 
	// Abandonné pour le moment
	
	var inputs, index;
	inputs = document.getElementsByTagName('input');
	var error=0;
	for (index = 0; index < inputs.length; ++index) {
		if (inputs[index].value < 0 ||
			inputs[index].value == null ||
			inputs[index].value == '') {
				alert(inputs[index].name);
				$('#'+inputs[index].name).css({"background-color": "#FF0000", "color": "#FFF"});
				error++;
		} else {
			$('#'+inputs[index].name).css({"background-color": "#FFF", "color": "#000"});
		}
	}

	if (error != 0) {
		$("#BlocChar").hide();
	} else {
		$("#BlocChar").show();
	}
	*/
	init();
});
function refreshChars() {
	var pas
	// Clear
	for (pas = 1; pas <= 5; pas++) {
		removeData(WaterChar);
		removeData(WattChar);
		removeData(CostChar);
	}
	for (pas = 1; pas <= 5; pas++) {
		// Water Char
		addData(WaterChar, pas, $( "#WaterShowerPerYear" ).text()*0.001*pas, $( "#WaterShowerloopPerYear" ).text()*0.001*pas) ;
		// Watt char
		addData(WattChar, pas, $( "#WattShowerPerYear" ).text()*pas, $( "#WattShowerloopPerYear" ).text()*pas) ;
		// Cost char
		addData(CostChar, pas, $( "#CostGlobalShowerPerYear" ).text()*pas, ($( "#CostGlobalShowerloopPerYear" ).text()+$( '#ShowerloopAnnualMaintenancePrice').text())*pas + parseFloat($( '#ShowerloopBuyPrice').text()));
		
	}
	
}
function calcRoi() {
	var resultRoi=parseFloat($( "#ShowerloopBuyPrice" ).text())/(parseFloat($( "#CostGlobalShowerPerYear" ).text())-parseFloat($( "#CostGlobalShowerloopPerYear" ).text())-parseFloat($( "#ShowerloopAnnualMaintenancePrice" ).text()));
	$( "#roi" ).text(resultRoi.toFixed(2));
}

function calcCostGlobalShowerloopPerYear() {
	var resultCostGlobalShowerloopPerYear=parseFloat($( "#CostWattShowerloopPerYear" ).text())+parseFloat($( "#CostWaterShowerloopPerYear" ).text());
	$( "#CostGlobalShowerloopPerYear" ).text(resultCostGlobalShowerloopPerYear.toFixed(2));
}
function calcCostWattShowerloopPerYear() {
	var resultCostWattShowerloopPerYear=parseFloat($( "#WattShowerloopPerYear" ).text()) * parseFloat($( "#WattCost" ).text());
	$( "#CostWattShowerloopPerYear" ).text(resultCostWattShowerloopPerYear.toFixed(0));
}
function calcWattShowerloopPerYear() {
	var resultWattShowerloopPerYear=parseFloat($( "#WattPerShowerloopTotal" ).text()) * parseFloat($( "#NbPeaple" ).val()) * parseFloat($( "#NbShowerPerPeaplePerWeek" ).val()) * 52;
	$( "#WattShowerloopPerYear" ).text(resultWattShowerloopPerYear.toFixed(0));
}
function calcCostWaterShowerloopPerYear() {
	var resultCostWaterShowerloopPerYear=parseFloat($( "#WaterShowerloopPerYear" ).text()) * 0.001 * parseFloat($( "#WaterCost" ).text());
	$( "#CostWaterShowerloopPerYear" ).text(resultCostWaterShowerloopPerYear.toFixed(0));
}
function calcWaterShowerloopPerYear() {
	var resultWaterShowerloopPerYear=parseFloat($( "#WaterPerShowerloop" ).text()) * parseFloat($( "#NbPeaple" ).val()) * parseFloat($( "#NbShowerPerPeaplePerWeek" ).val()) * 52;
	$( "#WaterShowerloopPerYear" ).text(resultWaterShowerloopPerYear.toFixed(0));
}


function calcCostGlobalPerShowerloop() {
	var resultCostGlobalPerShowerloop=parseFloat($( "#CostWattPerShowerloop" ).text())+parseFloat($( "#CostWaterPerShowerloop" ).text());
	$( "#CostGlobalPerShowerloop" ).text(resultCostGlobalPerShowerloop.toFixed(2));
}
function calcWattPerShowerCompare() {
	var resultWattPerShowerCompare=(parseFloat($( "#WattPerShower" ).text()) - parseFloat($( "#WattPerShowerloopTotal" ).text())) / parseFloat($( "#WattPerShower" ).text()) * 100;
	$( "#WattPerShowerCompare" ).text(resultWattPerShowerCompare.toFixed(0));
}

function calcCostWattPerShowerloop() {
	var resultCostWattPerShowerloop=parseFloat($( "#WattPerShowerloopHotWater" ).text())*parseFloat($( "#WattCost" ).text())+parseFloat($( "#WattPerShowerloopOperation" ).text())*parseFloat($( "#PriceEnergyelectric" ).val());
	$( "#CostWattPerShowerloop" ).text(resultCostWattPerShowerloop.toFixed(2));	
}
function calcWattPerShowerloopTotal() {
	var resultWattPerShowerloopTotal=parseFloat($( "#WattPerShowerloopOperation" ).text()) + parseFloat($( "#WattPerShowerloopHotWater" ).text());
	$( "#WattPerShowerloopTotal" ).text(resultWattPerShowerloopTotal.toFixed(2));
}
function calcWattPerShowerloopOperation() {
	var resultWattPerShowerloopOperation=parseFloat($( "#WattPerShowerloop" ).val()) * (parseFloat($( "#ShowerTimePerPeaple" ).val()) / 60 ) / 1000;
	$( "#WattPerShowerloopOperation" ).text(resultWattPerShowerloopOperation.toFixed(2));
}
function calcWattPerShowerloopHotWater () {
	var energyForWaterPerShowerloop = ((parseFloat($( "#ShowerTemp" ).val()) - parseFloat($( "#WaterTemp" ).val()) ) * ( parseFloat($( "#EnergyTheoretical1l1degForShower" ).text()) / 1000 ) * parseFloat($( "#WaterPerShowerloop" ).text()));
	//console.log('Energy for water per showerloop : '+ energyForWaterPerShowerloop);
	var lossDeg = 3;
	var energyLossDegForOtherCycle=lossDeg * parseFloat($( "#EnergyTheoretical1l1degForShower" ).text()) * parseFloat($( "#pumpFlow" ).val()) * parseFloat($( "#ShowerTimePerPeaple" ).val());
	//console.log('Loss of ' + lossDeg + '°C between the shower head and the siphon : '+ energyLossDegForOtherCycle);
	// Coefficient d’émission de chaleur de la tuyauterie pour un diamètre extérieur de 20mm (RT2000) – k
	// Heat Emission Coefficient of Piping for an Outside Diameter of 20mm
	var k = 0.86;
	// Total length of pipe L
	var l = 3 ;
	// Air temperature
	var Tair = 18 ;
	// Convection losses: kxLx (Water-Tair)
	var convecLoss=k*l*(parseFloat($( "#ShowerTemp" ).val())-Tair);
	//console.log('Convection losses: (with an air at '+Tair+'°C and a length of ' + l + ': '+convecLoss + 'Wh')
	// Energy lost by convection piping (Fconv)
	var energyLossConvectionPiping=convecLoss*parseFloat($( "#ShowerTimePerPeaple" ).val())/60
	//console.log('Energy lost by convection piping : '+energyLossConvectionPiping+'Wh');
	// Energy to compensate for evaporation losses in the shower and convection in the piping 
	var energyCompensation=(energyLossConvectionPiping+energyLossDegForOtherCycle)/1000
	//console.log('Energy to compensate for evaporation losses in the shower and convection in the piping : ' + energyCompensation + 'kWh');
	var resultWattPerShowerloopHotWater=energyForWaterPerShowerloop+energyCompensation
	$( "#WattPerShowerloopHotWater" ).text(resultWattPerShowerloopHotWater.toFixed(2));
}
function calcWaterPerShowerCompare() {
	var resultWaterPerShowerCompare=(parseFloat($( "#WaterPerShower" ).text()) - parseFloat($( "#WaterPerShowerloop" ).text())) / parseFloat($( "#WaterPerShower" ).text()) * 100;
	$( "#WaterPerShowerCompare" ).text(resultWaterPerShowerCompare.toFixed(0));
}
function calcCostWaterPerShowerloop() {
	var resultCostWaterPerShowerloop=parseFloat($( "#WaterCost" ).text()) * 0.001 * parseFloat($( "#WaterPerShowerloop" ).text());
	$( "#CostWaterPerShowerloop" ).text(resultCostWaterPerShowerloop.toFixed(3));
}
function showerloopeCost() {
	var ModeleData = $('#ShowerloopModel').val().split('|');	
	$( '#ShowerloopBuyPrice').text(ModeleData[1]);
	$( '#ShowerloopAnnualMaintenancePrice').text(ModeleData[2]);
	$( "#WaterPerShowerloop" ).text(ModeleData[3]);
	$( "#WattPerShowerloop" ).val(ModeleData[4]);
	$( "#pumpFlow" ).val(ModeleData[5]);
}
function calcCostGlobalShowerPerYear() {
	var resultCostGlobalShowerPerYear=parseFloat($( "#CostWattShowerPerYear" ).text())+parseFloat($( "#CostWaterShowerPerYear" ).text());
	$( "#CostGlobalShowerPerYear" ).text(resultCostGlobalShowerPerYear.toFixed(2));
}
function calcCostWattShowerPerYear() {
	var resultCostWattShowerPerYear=parseFloat($( "#WattShowerPerYear" ).text()) * parseFloat($( "#WattCost" ).text());
	$( "#CostWattShowerPerYear" ).text(resultCostWattShowerPerYear.toFixed(0));
}
function calcWattShowerPerYear() {
	var resultWattShowerPerYear=parseFloat($( "#WattPerShower" ).text()) * parseFloat($( "#NbPeaple" ).val()) * parseFloat($( "#NbShowerPerPeaplePerWeek" ).val()) * 52;
	$( "#WattShowerPerYear" ).text(resultWattShowerPerYear.toFixed(0));
}
function calcCostWaterShowerPerYear() {
	var resultCostWaterShowerPerYear=parseFloat($( "#WaterShowerPerYear" ).text()) * 0.001 * parseFloat($( "#WaterCost" ).text());
	$( "#CostWaterShowerPerYear" ).text(resultCostWaterShowerPerYear.toFixed(0));
}
function calcWaterShowerPerYear() {
	var resultWaterShowerPerYear=parseFloat($( "#WaterPerShower" ).text()) * parseFloat($( "#NbPeaple" ).val()) * parseFloat($( "#NbShowerPerPeaplePerWeek" ).val()) * 52;
	$( "#WaterShowerPerYear" ).text(resultWaterShowerPerYear.toFixed(0));
}

function calcCostGlobalPerShower() {
	var resultCostGlobalPerShower=parseFloat($( "#CostWattPerShower" ).text())+parseFloat($( "#CostWaterPerShower" ).text());
	$( "#CostGlobalPerShower" ).text(resultCostGlobalPerShower.toFixed(2));
}
function calcCostWattPerShower() {
	var resultCostWattPerShower=parseFloat($( "#WattPerShower" ).text())*parseFloat($( "#WattCost" ).text());
	$( "#CostWattPerShower" ).text(resultCostWattPerShower.toFixed(2));
}
function calcWattCost() {
	$( "#WattCost" ).text($( "#PriceEnergy" + $( "#HotWaterSystem" ).val()).val());
}
function calcWattPerShower() {
	var resultWattPerShower=(parseFloat($( "#ShowerTemp" ).val()) - parseFloat($( "#WaterTemp" ).val()) ) * ( parseFloat($( "#EnergyTheoretical1l1degForShower" ).text()) / 1000 ) * parseFloat($( "#WaterPerShower" ).text());
	$( "#WattPerShower" ).text(resultWattPerShower.toFixed(2));
}
function calcEnergyTheoretical1l1degForShower() {
	var resultEnergyTheoretical1l1degForShower=parseFloat(1.162) * (100 - parseFloat($( "#YieldInstallHotWater" ).val())) / 100 + parseFloat(1.162);
	$( "#EnergyTheoretical1l1degForShower" ).text(resultEnergyTheoretical1l1degForShower.toFixed(2));
}
function calcCostWaterPerShower() {
	var resultCostWaterPerShower=parseFloat($( "#WaterCost" ).text()) * 0.001 * parseFloat($( "#WaterPerShower" ).text());
	$( "#CostWaterPerShower" ).text(resultCostWaterPerShower.toFixed(2));
}
function calcWaterPerShower () {
	var resultWaterPerShower=parseFloat($( "#ShowerFlow" ).val()) * parseFloat($( "#ShowerTimePerPeaple" ).val());
	$( "#WaterPerShower" ).text(resultWaterPerShower.toFixed(0));
}
function calcWaterCost() {
	var resultWaterCost=parseFloat($( "#WaterCostShop" ).val()) + parseFloat($( "#CleanWaterCost" ).val());
	$( "#WaterCost" ).text(resultWaterCost.toFixed(2));
	$( "#WaterCostTooltip" ).attr('title', $( "#WaterCostShop" ).val() + ' <?= $config_ini['general']['currency'] ?>/m&sup3; + ' + $( "#CleanWaterCost" ).val() + ' <?= $config_ini['general']['currency'] ?>/m&sup3;');
}
function changeNiveau() {
	// Peut de détail
	if ($( "#Verbose" ).val() == 1) {
		$( "#BlocData" ).hide();
		$( "#EjOnglet" ).hide();
		$( ".form.ShowerTemp" ).hide();
		$( ".form.ShowerFlow" ).hide();
		$( ".form.YieldInstallHotWater" ).hide();
		$("#txtWattCost").hide();
		$("#txtWaterPerShower").hide();
		$("#txtWattPerShower").hide();
		$("#txtCostGlobalPerShower").hide();
		$("#txtShowerPerYear").hide();
		$("#txtShowerloopModel").hide();
		$("#txtWaterPerShowerloop").hide();
		$("#txtWattPerShowerloopHotWater").hide();
		$("#txtWattPerShowerloopOperation").hide();
		$("#txtWattPerShowerloopTotal").hide();
		$("#txtCostGlobalPerShowerloop").hide();
		$("#txtShowerloopPerYear").hide();
		$("#contrib").hide();		
	// intermédiaire (2)
	} else if  ($( "#Verbose" ).val() == 2) {
		$( "#BlocData" ).hide();
		$( ".form.ShowerTemp" ).show();
		$( ".form.ShowerFlow" ).hide();
		$( ".form.YieldInstallHotWater" ).hide();
		$("#txtWattCost").hide();
		$("#txtWaterPerShower").show();
		$("#txtWattPerShower").show();
		$("#txtCostGlobalPerShower").show();
		$("#txtShowerPerYear").show();
		$("#txtShowerloopModel").hide();
		$("#txtWaterPerShowerloop").show();
		$("#txtWattPerShowerloopHotWater").hide();
		$("#txtWattPerShowerloopOperation").hide();
		$("#txtWattPerShowerloopTotal").show();
		$("#txtCostGlobalPerShowerloop").show();
		$("#txtShowerloopPerYear").show();
		$("#contrib").show();
	// maximum (3)
	} else if ($( "#Verbose" ).val() == 3) {
		$( "#BlocData" ).show();
		$( ".form.ShowerTemp" ).show();
		$( ".form.ShowerFlow" ).show();
		$( ".form.YieldInstallHotWater" ).show();
		$("#txtWattCost").show();
		$("#txtWaterPerShower").show();
		$("#txtWattPerShower").show();
		$("#txtCostGlobalPerShower").show();
		$("#txtShowerPerYear").show();
		$("#txtShowerloopModel").show();
		$("#txtWaterPerShowerloop").show();
		$("#txtWattPerShowerloopHotWater").show();
		$("#txtWattPerShowerloopOperation").show();
		$("#txtWattPerShowerloopTotal").show();
		$("#txtCostGlobalPerShowerloop").show();
		$("#txtShowerloopPerYear").show();
		$("#contrib").show();
	}
}

$(document).ready(function() {
	init();
}); 

function init() {
	$( "#shareUrl" ).hide();
	$( "#shareSocial" ).hide();
	$( "#formAddEditConf" ).hide();
	changeNiveau();
	calcWaterCost();
	calcWaterPerShower();
	calcCostWaterPerShower();
	calcEnergyTheoretical1l1degForShower();
	calcWattPerShower();
	calcWattCost();
	calcCostWattPerShower();
	calcCostGlobalPerShower();
	calcWaterShowerPerYear();
	calcCostWaterShowerPerYear();
	calcWattShowerPerYear();
	calcCostWattShowerPerYear();
	calcCostGlobalShowerPerYear();
	showerloopeCost();
	calcCostWaterPerShowerloop();
	calcWaterPerShowerCompare();
	calcWattPerShowerloopHotWater();
	calcWattPerShowerloopOperation();
	calcWattPerShowerloopTotal();
	calcCostWattPerShowerloop();
	calcWattPerShowerCompare();
	calcCostGlobalPerShowerloop();
	calcWaterShowerloopPerYear();
	calcCostWaterShowerloopPerYear();
	calcWattShowerloopPerYear();
	calcCostWattShowerloopPerYear();
	calcCostGlobalShowerloopPerYear();
	calcRoi();
	refreshChars();
}
</script>

