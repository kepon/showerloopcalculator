# ShowerloopCalculator

Tool to calculate the gain of a showerloop (shower with water recycling) compared to a conventional shower (in energy, water, money ...) 

**The project / calculator is accessible at: http://showerloopcalculator.zici.fr/**

This calculator is based on the hardware and open source project: http://showerloop.org/

Source : https://forge.zici.fr/source/ShowerloopCalculator/

### Installing the tool on my site:

#### Required for the operation / installation of the

  * PHP (5.6 minimum) + GeoIP +Gettext
  * Apache/Nginx (or other web server, shared hosting service ...) with URL Rewriting
  
#### Installation

Download and unzip the master zip file: https://github.com/kepon85/CalcPvAutonome/archive/master.zip

Make it accessible from your http server and customize the value of the config/*.ini file

### Auteur / contributeur

  - David Mercereau ([david #arobase# mercereau #point# info](http://david.mercereau.info/contact/))
  - Robin Guitton (robin #point# guitton #arobase# zaclys #point# net)

### License BEERWARE

As long as you retain this notice you can do whatever you want with this stuff. If we meet some day, and you think this stuff is worth it, you can buy me a beer in return Poul-Henning Kamp
